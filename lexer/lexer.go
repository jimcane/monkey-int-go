package lexer

import "monkey-int-go/token"

type Lexer struct {
	input        string
	position     int
	readPosition int
	ch           byte
}

func New(input string) *Lexer {
	return &Lexer{input: input}
}

func (l *Lexer) NextToken() token.Token {

	return token.Token{Type: token.PLUS, Literal: "+"}
}
